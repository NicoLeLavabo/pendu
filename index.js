/**
 * Get letter chosen by the user in prompt
 * @author Nicolas
 * @return letter
 */
function demande() {
    return prompt('Quel est votre lettre ?');
}

/**
 * Get random number between min max
 * @author Nicolas
 * @param {entier} min 
 * @param {entier} max 
 * @return random result beetween between min max
 */
function randomMinMax(min, max) {
    let result = Math.round(Math.random() * (max - min) + min);
    return result;
}

/**
 * Principal function of hangman game, show result in console and show alert() if win, loose or wrong letter 
 * @author Nicolas
 */
function pendu() {
    let tabMots = ['test', 'ordinateur', 'souris', 'clavier', 'ecran', 'crayon', 'sacoche', 'masque', 'cable'];
    let result = randomMinMax(0, tabMots.length - 1);
    let tabReponse = tabMots[result];
    let tabUser = [];
    //console.log(tabReponse);
    let chance = 6;
    let trouver = false;
    let TabReponseEclate = [];
    let usedLetters = [];

    //afficher le mot avec des underscore
    for (i = 0; i < tabReponse.length; i++) {
        tabUser.push('_');
    }

    //1 lettre une case tableau
    for (i = 0; i < tabReponse.length; i++) {
        TabReponseEclate.push(tabReponse[i]);

    }
    console.log(tabUser);
    while (trouver == false && chance !== 0) {
        userLetter = demande();
        usedLetters.push(userLetter);
        console.log('used letters ' + usedLetters)
        for (i = 0; i <= tabReponse.length; i++) {
            if (TabReponseEclate.toString() == tabUser.toString()) {
                trouver = true;
                alert('YOUHOU gg');
            }
            else if (userLetter === tabReponse[i]) {
                tabUser[i] = userLetter;
                console.log(tabUser);
            }

        }
        if (!tabReponse.includes(userLetter)) {
            chance--;
            alert('Mauvaise lettre, il vous reste ' + chance + ' essais');
        }
        if (chance === 0) {
            alert('PERDU ! Le mot a trouver était ' + tabReponse);
        }
    }
}